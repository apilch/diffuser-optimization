function gamma=gamma_fun(n,f,fie,N,d,w)

T=N*(d+w);
c=343;
k=2*pi*f/c;
lambda0=c./f;
if n==0
    gamma=1i*k*cos(fie);
else
gamma=k.*((sin(fie)+n*lambda0/T).^2-1).^(1/2);  %Mechel WAD (25)
end