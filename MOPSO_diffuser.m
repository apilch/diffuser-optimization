function [wynik]=MOPSO_diffuser(N,depth,depth_sk,rodzaj,cost,nPop,it)
tic
%% Problem Definition
% N=11;
% depth=0.15; %maksymalna %g��boko�� studzienek
% depth_sk=0.12; %maksymalna %g��boko�� skrajnych studzienek

% ile_komb=8;
% rodzaj='alpha_lustro';
% cost='opt_rozpraszacze_opera_RozneKaty_Euronoise2';
% it=11;

% nPop=6;            % Population Size
MaxIt=it;           % Maximum Number of Iterations
CostFunction=str2func(cost);      % Cost Function
max_depth=[depth_sk;repmat(depth,N-2,1);depth_sk];
nVar=N;             % Number of Decision Variables
VarSize=[1 nVar];   % Size of Decision Variables Matrix

VarMin=([zeros(N,1)])';               % Lower Bound of Variables
VarMax=(ones(N,1).*max_depth)';        % Upper Bound of Variables

%% MOPSO Parameters


nRep=round(0.8*nPop);            % Repository Size
w=0.5;              % Inertia Weight
wdamp=0.99;         % Intertia Weight Damping Rate
c1=1;               % Personal Learning Coefficient
c2=2;               % Global Learning Coefficient

nGrid=20;            % Number of Grids per Dimension
alpha=0.1;          % Inflation Rate

beta=2;             % Leader Selection Pressure
gamma=2;            % Deletion Selection Pressure

mu=0.1;             % Mutation Rate

%% Initialization

empty_particle.Position=[];
empty_particle.Velocity=[];
empty_particle.Cost=[];
empty_particle.Best.Position=[];
empty_particle.Best.Cost=[];
empty_particle.IsDominated=[];
empty_particle.GridIndex=[];
empty_particle.GridSubIndex=[];

pop=repmat(empty_particle,nPop,1);
NewSol=pop;
dane=zeros(nPop,N);
% for i=1:nPop
%       dane(i,1:nVar-ile)=randraw('uniform',[VarMin,VarMax],N-ile);
%       dane(i,nVar-ile+1:nVar)=round(randraw('uniform',[1,8],1));
% end

for i=1:N
      dane(:,i)=randraw('uniform',[VarMin(i),VarMax(i)],nPop);

end
dane(:,end)=round(dane(:,end));

% if exist('max_sk','var')
%     dane(dane(:,1)>max_sk,1)=max_sk;
%     dane(dane(:,end-ile)>max_sk,end-ile)=max_sk;
% 
% end

parfor i=1:nPop
    pop(i).Position=dane(i,:);
%     pop(i).Position;
   
    pop(i).Velocity=zeros(VarSize);
    
    pop(i).Cost=CostFunction(pop(i).Position,rodzaj);
    
    
    % Update Personal Best
    pop(i).Best.Position=pop(i).Position;
    pop(i).Best.Cost=pop(i).Cost;
end

% Determine Domination
pop=DetermineDomination(pop);

rep=pop(~[pop.IsDominated]);

Grid=CreateGrid(rep,nGrid,alpha);

for i=1:numel(rep)
    rep(i)=FindGridIndex(rep(i),Grid);
end


%% MOPSO Main Loop
for it=1:MaxIt
    parfor i=1:nPop
        leader=SelectLeader(rep,beta);
        
        pop(i).Velocity = w*pop(i).Velocity ...
            +c1*rand(VarSize).*(pop(i).Best.Position-pop(i).Position) ...
            +c2*rand(VarSize).*(leader.Position-pop(i).Position);
        
        pop(i).Position = pop(i).Position + pop(i).Velocity;
        
        pop(i).Position = max(pop(i).Position, VarMin);
        pop(i).Position = min(pop(i).Position, VarMax);
        
        pop(i).Position(end)=round(pop(i).Position(end));
        pop(i).Cost = CostFunction(pop(i).Position,rodzaj);
        
        % Apply Mutation
        pm=(1-(it-1)/(MaxIt-1))^(1/mu);
        if rand<pm
            NewSol(i).Position=Mutate_FPGS(pop(i).Position,pm,VarMin,VarMax,depth_sk);
            NewSol(i).Cost=CostFunction(NewSol(i).Position,rodzaj);
            if Dominates(NewSol(i),pop(i))
                pop(i).Position=NewSol(i).Position;
                pop(i).Cost=NewSol(i).Cost;

            elseif Dominates(pop(i),NewSol(i))
                % Do Nothing

            else
                if rand<0.5
                    pop(i).Position=NewSol(i).Position;
                    pop(i).Cost=NewSol(i).Cost;
                end
            end
        end
        
        if Dominates(pop(i),pop(i).Best)
            pop(i).Best.Position=pop(i).Position;
            pop(i).Best.Cost=pop(i).Cost;
            
        elseif Dominates(pop(i).Best,pop(i))
            % Do Nothing
            
        else
            if rand<0.5
                pop(i).Best.Position=pop(i).Position;
                pop(i).Best.Cost=pop(i).Cost;
            end
        end
        
    end
    
    % Add Non-Dominated Particles to REPOSITORY
    rep=[rep
         pop(~[pop.IsDominated])]; %#ok
    
    % Determine Domination of New Resository Members
    rep=DetermineDomination(rep);
    
    % Keep only Non-Dminated Memebrs in the Repository
    rep=rep(~[rep.IsDominated]);
    
    % Update Grid
    Grid=CreateGrid(rep,nGrid,alpha);

    % Update Grid Indices
    for i=1:numel(rep)
        rep(i)=FindGridIndex(rep(i),Grid);
    end
    
    % Check if Repository is Full
    if numel(rep)>nRep
        
        Extra=numel(rep)-nRep;
        for e=1:Extra
            rep=DeleteOneRepMemebr(rep,gamma);
        end
        
    end
    
    % Plot Costs
  %  figure(1);
  %  PlotCosts(pop,rep);
  %  pause(0.01);
    
    % Show Iteration Information
    disp(['Iteration ' num2str(it) ': Number of Rep Members = ' num2str(numel(rep))]);
    
    % Damping Inertia Weight
    w=w*wdamp;
    
end

wynik.pop=pop;
wynik.rep=rep;
wynik.Cost=CostFunction;
wynik.nPop=nPop;
wynik.it=it;
wynik.rodzaj=rodzaj;
toc
% save MOPSO_FPGS5