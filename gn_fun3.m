function gn=gn_fun3(n,G,N,d,w)

%oparte na Mechel WAD (34)

T=N*(d+w);

%% bez sinc

s=zeros(1,length(n));
% for ii=1:length(n)
    
     if any(n==0)
           s(n==0)=sum(bsxfun(@times,G,exp(-1i*bsxfun(@times,n(n==0),pi*(2*(0:(N-1))+1)/N))),2);
     end
     
     if any(n~=0)
           s(n~=0)=sum(bsxfun(@times,rot90(G,3),exp(-1i*bsxfun(@times,n(n~=0),pi*(2*(0:(N-1))'+1)/N))),1).*sin(2*n(n~=0)*pi*d/2/T)./(2*n(n~=0)*pi*d/2/T);
     end


% end
 gn=d/T*s; %wzor 34 z Mechel WAD
%% z sinc
% for ii=1:length(n)
%     for k=0:N-1
%     s(k+1)=G(k+1)*exp(-1i*n(ii)*pi*(2*k+1)/N)*sinc(2*n(ii)*d/2/T);
%     end
%   gn(ii)=d/T*sum(s); %wzor 34 z Mechel WAD
%   clear s
% end