function [d,dn,deltac,ps,psp]=diff_fun_kat_padania_3D(tk,h,f,psitheta,psiphi,psp)

rr=5;
rr0=10;
w=h;
N=length(tk);

psi(1)=psitheta;
psi(2)=psiphi;

%zmiana theta do 180 stopni !!!
theta=-pi/2:(5*pi/180):pi/2;
% theta=0:(5*pi/180):pi/2-5*pi/180;

%zmiana phi na 0 stopni !!!
phi=0;%:5*pi/180:2*pi-5*pi/180;
lmin=343/(max(f))/6;
stud=ceil(w/lmin);
% 
hn=zeros(length(tk)*stud+1,length(tk)*stud+1);

for n=1:N
   hn(:,(n-1)*stud+1:stud*n)=tk(n);
end

ys=(-size(hn,2)/2:1:size(hn,2)/2)*w/stud;
xs=ys;
hn(:,end+1)=hn(:,end);
hn(end+1,:)=hn(end,:);

%odwracanie x i y
hn=hn';
c=343;
k=2*pi*f'/c;
R=zeros(length(f),size(hn,1),size(hn,2));
Rp=R;
for i=1:length(k)
R(i,:,:)= exp(-1i*k(i)*2*hn);
Rp(i,:,:)=exp(-1i*k(i)*2*zeros(size(hn)));
end
dys(1)=xs(2)-xs(1);
dys(2)=ys(2)-ys(1);

ps=zeros(length(f),length(theta),length(phi));
if nargin<6
psp=zeros(length(f),length(theta),length(phi));
end
% wzor dokladny

r0=[rr0*sin(psi(1))*cos(psi(2)),rr0*sin(psi(1))*sin(psi(2)),rr0*cos(psi(1))]';

r(:,:,1)=rr*sin(theta')*cos(phi);
r(:,:,2)=rr*sin(theta')*sin(phi);
r(:,:,3)=rr*cos(theta')*ones(1,length(phi));

rsr0=((xs-r0(1))'.^2+(ys-r0(2)).^2+r0(3).^2).^(1/2);    



flag=3;
for it=1:length(theta)
    for ip=1:length(phi)    
        rrs=((r(it,ip,1)-xs)'.^2+(r(it,ip,2)-ys).^2+r(it,ip,3).^2).^(1/2);
   ps(:,it,ip)=   -1i*k.*sum(sum(greens(k,rsr0,flag).*(1+squeeze(R(:,:,:))).* greens(k,rrs,flag).*(cos(theta(it))+cos(psi(1)).*(squeeze(R(:,:,:))-1)./ (squeeze(R(:,:,:))+1))*dys(1)*dys(2),3),2);
  if nargin<6
   psp(:,it,ip)=  -1i*k.*sum(sum(greens(k,rsr0,flag).*(1+squeeze(Rp(:,:,:))).*greens(k,rrs,flag).*(cos(theta(it))+cos(psi(1)).*(squeeze(Rp(:,:,:))-1)./(squeeze(Rp(:,:,:))+1))*dys(1)*dys(2),3),2);
  end

    end
    
end

y=length(theta);

d=zeros(length(phi),length(f));
if nargin<6
dp=d;
end
deltac=d;
for ip=1:length(phi)
d(ip,:)= (sum(abs(ps(:,:,ip)),2).^2-sum(abs(ps(:,:,ip)).^2,2) ) ./ ( (y-1)*sum(abs(ps(:,:,ip)).^2,2) );

if length(phi)==1
dp=(sum(abs(psp),2).^2-sum(abs(psp).^2,2) ) ./ ( (y-1)*sum(abs(psp).^2,2)) ;
dp=dp';
else
dp(ip,:)=( sum(abs(psp(:,:,ip)),2).^2-sum(abs(psp(:,:,ip)).^2,2) ) ./ ( (y-1)*sum(abs(psp(:,:,ip)).^2,2)) ;
end

for icz=1:length(f)
     if length(phi)==1
        deltac(ip,icz)=1-((abs(sum(ps(icz,:)*(psp(icz,:)')))).^2)./(sum(abs(ps(icz,:)).^2)*sum(abs(psp(icz,:)).^2));
     else 
      deltac(ip,icz)=1-((abs(sum(ps(icz,:,ip)*(psp(icz,:,ip)'),2))).^2)./(sum(abs(ps(icz,:,ip)).^2,2)*sum(abs(psp(icz,:,ip)).^2,2));
  
    end
end
end

dn=(d-dp)./(1-dp);


