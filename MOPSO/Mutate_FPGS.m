%
% Copyright (c) 2015, Yarpiz (www.yarpiz.com)
% All rights reserved. Please read the "license.txt" for license terms.
%
% Project Code: YPEA121
% Project Title: Multi-Objective Particle Swarm Optimization (MOPSO)
% Publisher: Yarpiz (www.yarpiz.com)
% 
% Developer: S. Mostapha Kalami Heris (Member of Yarpiz Team)
% 
% Contact Info: sm.kalami@gmail.com, info@yarpiz.com
%

function xnew=Mutate_FPGS(x,pm,VarMin,VarMax,max_sk)

    nVar=numel(x);
    j=randi([1 nVar]);

    dx=pm*(VarMax-VarMin);
    
    lb=x(j)-dx;
    
    
    lb(lb<VarMin)=VarMin(lb<VarMin);
    
    ub=x(j)+dx;
    ub(ub>VarMax)=VarMax(ub>VarMax);
    
    xnew=x;
    xnew(j)=unifrnd(lb(j),ub(j));
if xnew(1)>max_sk
    xnew(1)=max_sk;
end
if xnew(end-1)>max_sk
    xnew(end-1)=max_sk;
end
end