function [OUT]=MODE_diffuser(N,depth,depth_sk,rodzaj,cost,pop,it)
%% Variables regarding the optimization problem
% N=11;
% depth=0.15; maksymalna g��boko�� studzienek
% depth_sk=0.12 maksymalna g��boko�� skrajnych studzienek

max_depth=[depth_sk;repmat(depth,N-2,1);depth_sk];
% rodzaj = 'alpha_lustro'; - parametry obliczane jako kryterium
% optymalizacji, 'd_alpha',  case 'd_delta', 'alpha_lustro', 'alpha_bok', 'd_lustro'
% cost - funkcja koszt�w
% pop - ilo�� cz�onk�w populacji
% it - ilo�� iteracji. 


MODEDat.NOBJ = 2;                          % Number of objectives
MODEDat.NRES = 0;                          % Number of constraints
MODEDat.NVAR = N;                          % Number of decision variables
MODEDat.mop = str2func('CostFunction');    % Cost function
MODEDat.CostProblem=cost;               % Cost function instance
% MODEDat.max_sk=0.015;
MODEDat.FieldD =[zeros(MODEDat.NVAR,1)...
                   ones(MODEDat.NVAR,1).*max_depth]; % Initialization bounds
MODEDat.Initial=[zeros(MODEDat.NVAR,1)...
                     ones(MODEDat.NVAR,1).*max_depth]; % Optimization bounds

%% Variables regarding the optimization algorithm
tic
MODEDat.XPOP = pop;% 5*MODEDat.NOBJ;             % Population size
MODEDat.Esc = 0.5;                         % Scaling factor
MODEDat.Pm= 0.4;                           % Croosover Probability
%
%% Other variables
MODEDat.InitialPop=[];                     % Initial population (if any)
MODEDat.MAXGEN =it;                            % Generation bound
MODEDat.MAXFUNEVALS = 500*MODEDat.NVAR...  % Function evaluations bound
    *MODEDat.NOBJ;                         
MODEDat.SaveResults='no';                 % Write 'yes' if you want to 
                                           % save your results after the
                                           % optimization process;
                                           % otherwise, write 'no';
%% Initialization (don't modify)
MODEDat.CounterGEN=0;
MODEDat.CounterFES=0;
MODEDat.rodzaj=rodzaj;
OUT=MODE_par(MODEDat,rodzaj);                         % Run the algorithm.
%%
% 
% $$e^{\pi i} + 1 = 0$$
% 
toc
