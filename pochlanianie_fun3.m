function [alfau]=pochlanianie_fun3(tk,h,w,fie,fc)

N=length(tk);
d=2*h; %szerokosc studzienki
c=343;

% fg1=fc(end).*(2^(1/6));

% 
% f0=100*2.^((-3:90)/12);
% 
% f0=f0(f0>min(fc/(2^(1/6))));

f0=fc;

T=N*(2*h+w);
kappa=1.401;
v=15.13*10^-6;

nsm(1)=ceil(-T/(c/f0(end))*(1+sin(fie)));
nsm(2)=floor(T/(c/f0(end))*(1-sin(fie)));

gr=-max([abs(nsm),2*N]);
gr(2)=-gr(1);

ns=zeros(2,length(f0));
wyn3=zeros(-gr(1)*2+1,length(f0));
alfa=zeros(1,length(f0));
rs2=alfa;
u2=alfa;

for cz=1:length(f0)
f=f0(cz);
om=2*pi*f;
k=om/c;
lambda0=c./f;

k0tk=tk*k; %Mechel Wide angle diffusser (33)


ns(1,cz)=ceil(-T/lambda0*(1+sin(fie)));
ns(2,cz)=floor(T/lambda0*(1-sin(fie)));

kv=(-1i*om/v).^(1/2);
ka0=(kappa*0.6977)^(1/2)*kv; %Mechel FoA, s.595, (3)
gamma=1i*((1+(kappa-1).*tan(ka0.*h)./(ka0.*h))./(1-tan(kv.*h)./(kv.*h))).^(1/2);%Mechel FoA, s.616, (8)
Z=1/(((1+0.401.*tan(ka0.*h)./(ka0.*h)).*(1-tan(kv.*h)./(kv.*h))).^(1/2));%Mechel FoA, s.616, (9)

G=tanh(gamma*k0tk)./Z;
lewa2=zeros(gr(2)-gr(1)+1);
prawa=zeros(1,gr(2)-gr(1)+1);
for m=gr(1):gr(2)
n=gr(1):gr(2);
lewa2(m-gr(1)+1,n-gr(1)+1)=(gn_fun3(-m-n,G,N,d,w)-1i*KronD1(m,-n).*gamma_fun(n,f,fie,N,d,w)/k);

prawa(m-gr(1)+1)=KronD(m,0)*cos(fie)-gn_fun3(-m,G,N,d,w);     
end
wyn3(:,cz)=lsqlin(lewa2,prawa);
ii=0;
s2=zeros(1,length(1-gr(1)+ns(1,cz):1-gr(1)+ns(2,cz)));

for il=1-gr(1)+ns(1,cz):1-gr(1)+ns(2,cz)
    ii=ii+1;
    if il==1+gr(2)
    s2(ii)=0;
    else
    s2(ii)=abs(wyn3(il,cz))^2*(1-(sin(fie)+(il+gr(1)-1)*lambda0/T)^2)^(1/2);
    end
end
alfa(cz)=1-abs(wyn3(-gr(1)+1,cz))^2-1/cos(fie)*sum(s2);
rs2(cz)=1/cos(fie)*sum(s2);
u2(cz)=rs2(cz)/(abs(wyn3(-gr(1)+1,cz))^2+rs2(cz));
end

fd=fc./(2^(1/6));
fg=fd.*2^(1/3);
alfau=zeros(length(fc),1);
for i=1:length(fc)
    alfau(i)=mean(alfa(f0>fd(i) & f0<fg(i)));
end

alfau=alfau';