function [g] = greens(k,r,d)
if d==3    
%     r=reshape(r,1,size(r,1),size(r,2));
r1(1,:,:)=r;
g = exp(-1j*repmat(r1,length(k),1,1).*repmat(k,1,size(r1,2),size(r1,3)))./(4*pi*repmat(k,1,size(r1,2),size(r1,3)));         %Green's function
else
    g=exp(-1i*k*r)./sqrt(k*r);
end  
end