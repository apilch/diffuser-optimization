function [J]=opt_rozpraszacze_opera_RozneKaty_Euronoise2(dane,rodzaj)

gl=0.15;
Wcalkowite=0.3;
% load 'operaplyta'

load plyta
% tabela=[1 1 1 1;2 1 1 2;3 1 2 2;4 1 2 1;5 2 1 1;6 2 2 1;7 2 2 2;8 2 1 2];
fc=[100 125 160 200 250 315 400 500 630 800 1000 1250 1600 2000 2500 3150 4000 5000];
% 100*2.^((-3:90)/12);
switch length(fc)
    case 4
pspor=psp;
    case 18
  pspor=psp1_3;
    case 94
        pspor=pspDok;
end

clear psp
tk=dane(1:end)';

N=length(tk);
ws=Wcalkowite/N;


tkp=[tk;tk(end:-1:1,1)];
psi=(0:20:60)/180*pi;

ps_lustro=zeros(length(psi),1);
% du2=zeros(length(fc),length(psi));
theta=-pi/2:(5*pi/180):pi/2;

for i=1:length(psi)
[d(:,i),dn(:,i),deltac(:,i),ps(:,:,i),psp(:,:,i)]=diff_fun_kat_padania_3D(tkp,ws,fc,-psi(i),0,pspor(:,:,i));


ps_lustro(i)=sum(sum(sum(abs(ps(:,find(round(theta,4)==round(psi(i),4))-1:find(round(theta,4)==round(psi(i),4))+1,i)))));  

end

if contains(rodzaj,'alpha')
alfa=pochlanianie_fun3(tkp',ws/2,0,0,fc);
alfasr=max(alfa)+mean(alfa);
end
dnsr=1-mean(mean(mean(dn)))+mean(std(dn(fc>500,:)));
deltasr=1-mean(mean(mean(deltac)));
ps_lustrosr=sum(ps_lustro)/sum(sum(sum(sum(abs(ps)))));

ps_bok= 1-  mean((sum(abs(ps(:,[1:17,21:37],1))+abs(ps(:,[1:21,25:37],2)),2)./sum(sum(abs(ps(:,:,1:2)),3),2)+sum(sum(abs(ps(:,10:22,3:4)),3),2)./sum(sum(abs(ps(:,:,3:4)),3),2)))/2;
%!!! Zmiana 
% ps_lustro=1-mean(ps_lustro);
% rozp2500=1-rozpD(fc==2500);
% rozpD1=1-mean(rozpD)+std(rozpD(4:end));

% rozpD2=1-mean(du2)+std(du2(4:end));
% lustro=mean(ps_lustro);
% rozpS1=1-mean(rozpS)+std(rozpD(4:end));

switch rodzaj
    case 'd_alpha'
J=[dnsr; alfasr];
    case 'd_delta'
 J=[dnsr; deltasr];     
    case 'alpha_lustro'
  J=[alfasr;ps_lustrosr ];  
    case 'alpha_bok'
    J=[ alfasr;ps_bok];   
  case 'd_lustro'
    J=[dnsr; ps_lustrosr];  
end


