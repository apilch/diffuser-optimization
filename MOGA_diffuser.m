function [eMOGA,ParetoFront,ParetoSet]=MOGA_diffuser(N,depth,depth_sk,rodzaj,cost,pop,it)
% clear
% N=11;
% depth=0.15; %maksymalna %g��boko�� studzienek
% depth_sk=0.12 %maksymalna %g��boko�� skrajnych studzienek
max_depth=[depth_sk;repmat(depth,N-2,1);depth_sk];

tic
eMOGA.objfun=cost;
% rodzaj='alpha_lustro';
% addpath /mnt/lustre/scratch2/people/plgapilch/Matlab/evMOGAtoolbox

eMOGA.objfun_dim=2;
eMOGA.searchspaceUB=(ones(N,1).*max_depth)';
% eMOGA.searchspaceLB=([zeros(N-1,1);1])';
eMOGA.searchspaceLB=([zeros(N,1)])';


eMOGA.Nind_P=pop;
eMOGA.Generations=it;
eMOGA.Nind_GA=round(pop*0.8);           % ile osobnik�w za ka�dym przej�ciem p�tli
eMOGA.param=rodzaj;
% eMOGA.max_sk=0.015;          % maksymalna g��boko�� skrajnych studzienek

% help evMOGA
[ParetoFront,ParetoSet,eMOGA]=evMOGA_parFPGS(eMOGA);

toc